package seedCrawler;

import java.net.SocketException;
import java.net.SocketTimeoutException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import org.jsoup.HttpStatusException;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import com.mongodb.BasicDBObject;
import com.mongodb.Bytes;
import com.mongodb.DB;
import com.mongodb.DBCollection;
import com.mongodb.DBCursor;
import com.mongodb.DBObject;
import com.mongodb.DuplicateKeyException;
import com.mongodb.MongoClient;
import com.mongodb.util.JSON;

import fetchingSeeds.DbSeeds;
import mongoDBConnection.GetConnection;
import removeSpecialChar.RemoveSpecialChar;
import robots.RobotChecks;

public class Crawler {

	private static final int MYTHREADS;

	static DBCollection domainDB;
	static DBCollection seedUrl;
	static DBCollection name;
	static DBCollection crawldatadb;
	static DBCollection sampleCrawlDataDB;
	static DBCollection nameStructureDB;
	static DBCollection settingsDB;
	static DBCollection counters;

	static ArrayList<String> nameStructure;

	static Date dNow;
	static SimpleDateFormat ft;

	static {

		MYTHREADS = 50;
		domainDB = GetConnection.connect("domain");
		seedUrl = GetConnection.connect("SeedUrl");
		name = GetConnection.connect("nameStructure");
		crawldatadb = GetConnection.connect("crawldata");
		sampleCrawlDataDB = GetConnection.connect("sampleCrawldata");
		nameStructureDB = GetConnection.connect("nameStructure");
		settingsDB = GetConnection.connect("settings");
		counters = GetConnection.connect("counters");

		nameStructure = new ArrayList<>();

		dNow = new Date();
		ft = new SimpleDateFormat("yyyy-MM-dd");

	}
	private ExecutorService executor;
	String url, domain, regex, noRegex, nextCrawlDate, type, moduleName;
	String systemName, subMain, bucket, keyword, description;
	int bucketNumber;
	String image, translate;
	boolean imageStatus = true;

	BasicDBObject query;
	DBCursor cursor;
	DBObject object;

	Runnable worker;
	DbSeeds seeds;

	long startTime;
	long endTime;

	public Crawler() {
		query = new BasicDBObject();
	}

	public void crawlSeeds(ArrayList<DbSeeds> crawlList) {
		executor = Executors.newFixedThreadPool(MYTHREADS);
		for (int i = 0; i < crawlList.size(); i++) {
			System.out.println("Seed is " + crawlList.get(i).url);
			url = crawlList.get(i).url;
			domain = crawlList.get(i).domainName;
			regex = crawlList.get(i).regex;
			noRegex = crawlList.get(i).noRegex;
			bucketNumber = crawlList.get(i).bucketNumber;
			nextCrawlDate = crawlList.get(i).nextCrawlDate;
			type = crawlList.get(i).type;
			moduleName = crawlList.get(i).moduleName;

			query.clear();
			query.put("domainName", domain);
			cursor = domainDB.find(query);
			while (cursor.hasNext()) {
				systemName = cursor.next().get("systemName").toString();
			}

			query.clear();
			query.put("systemName", systemName);
			cursor = settingsDB.find(query);
			while (cursor.hasNext()) {
				object = cursor.next();
				image = object.get("image").toString();
				translate = object.get("translate").toString();
			}

			if (image.contains("yes")) {
				imageStatus = false;
			} else {
				imageStatus = true;
			}

			/*
			 * System.out.println("Seed is "+url); System.out.println(
			 * "Regex is "+regex); System.out.println("No regex is "+noRegex);
			 */
			worker = new MyRunnable(url, domain, regex, noRegex, bucketNumber, nextCrawlDate, type, moduleName,
					imageStatus);
			// executor.submit(worker);
			executor.execute(worker);
		}
		executor.shutdown();
		// Wait until all threads are finish
		while (!executor.isTerminated()) {
			// System.out.println("Executer termination
			// "+executor.isTerminated());
		}
		System.out.println("Finished set of 100");

	}

	public static void main(String[] args) throws InterruptedException {

		new Crawler().JsoupMulti24();
	}

	public void JsoupMulti24() throws InterruptedException {

		System.out.println("Code has started again");
		startTime = System.currentTimeMillis();

		cursor = nameStructureDB.find();

		while (cursor.hasNext()) {
			object = cursor.next();
			nameStructure.add((String) object.get("structure"));
		}

		cursor = DbSeeds.dbSeedUrl();
		cursor.addOption(Bytes.QUERYOPTION_NOTIMEOUT);

		ArrayList<DbSeeds> crawlList = new ArrayList<>();
		int set = 0;
		// System.out.println("List size is " + listcrawl.size());
		while (cursor.hasNext()) {
			System.out.println("List size is " + crawlList.size());
			System.out.println("Set size is " + set);
			if (set < 100) {
				object = cursor.next();
				url = object.get("url").toString();
				domain = object.get("domainUrl").toString();
				subMain = object.get("subMain").toString();
				bucket = object.get("bucketNumber").toString();
				try {
					bucketNumber = Integer.parseInt(bucket);
				} catch (NumberFormatException e) {
					bucketNumber = 0;
				}
				nextCrawlDate = object.get("nextCrawlDate").toString();
				moduleName = (String) object.get("moduleName");
				type = (String) object.get("type");

				query.clear();
				query.put("domainName", domain);
				object = domainDB.findOne(query);

				keyword = object.get("keyword").toString();
				description = object.get("description").toString();
				regex = object.get("regex").toString();
				noRegex = object.get("noRegex").toString();

				seeds = new DbSeeds(url, domain, moduleName, nextCrawlDate, bucketNumber, type, description, keyword,
						regex, noRegex, subMain);
				crawlList.add(seeds);
				set = set + 1;
			} else {
				set = 0;
				System.out.println("List is 100 and calling threads");
				new Crawler().crawlSeeds(crawlList);
				crawlList.clear();
			}
		}
		if (crawlList.size() > 0)
			System.out.println("List is " + crawlList.size() + " and calling threads");
		new Crawler().crawlSeeds(crawlList);
		System.out.println("All seed urls are crawled");
		crawlList.clear();

		endTime = System.currentTimeMillis();

		long timeLeft = 3600000; // 1 hour in miliseconds
		int time = (int) (endTime - startTime);
		System.out.println("Program took " + time);
		System.out.println("Code execution is complete. " + timeLeft + " miliseconds are left for next crawl");
		Thread.sleep(timeLeft);
		/*
		 * timeLeft = timeLeft / 1000; timeLeft = timeLeft / 60; timeLeft =
		 * timeLeft / 60;
		 */

		JsoupMulti24();
	}

	public class MyRunnable implements Runnable {
		private String URL;
		private String authorityUrl, title;
		private String domain;
		private String regex, noRegex;
		private int bucketNumber;
		private String nextCrawlDate;
		private String type, moduleName;
		private boolean imageStatus;
		Document doc;
		Elements links = null;
		boolean classEnabled = false;
		boolean filters = true;
		boolean nofilters = true;
		boolean nameFlag = true;

		String noRegexString;
		String[] noRegexarray;

		BasicDBObject query = new BasicDBObject();
		DBCursor cursor;
		DBObject object;

		String smallTitle;
		String smallNameStructure;
		BasicDBObject insertObject = new BasicDBObject();

		MyRunnable(String URL, String domain, String regex, String noRegex, int bucketNumber, String nextCrawlDate,
				String type, String moduleName, boolean imageStatus) {
			this.URL = URL;
			this.domain = domain;
			this.regex = regex;
			this.noRegex = noRegex;
			this.bucketNumber = bucketNumber;
			this.nextCrawlDate = nextCrawlDate;
			this.type = type;
			this.moduleName = moduleName;
			this.imageStatus = imageStatus;
		}

		@Override
		public void run() {

			try {
				doc = Jsoup.connect(URL).userAgent("Mozilla||Chrome").timeout(0x927c0).get();
				try {
					updatenextCrawlDate(URL, bucketNumber, nextCrawlDate);
					// System.out.println("Connected to "+URL+" and next crawl
					// date is given");
				} catch (ParseException e1) {

					// e1.printStackTrace();
					System.out.println("Problem in updating next crawl date for " + URL);
				}

				if (regex.contains("class")) {
					links = doc.select(regex).select("a[href]");
					classEnabled = true;
				} else {
					links = doc.select("a[href]");
				}

				for (Element link : links) {
					authorityUrl = link.attr("abs:href");
					title = link.text();
					title = RemoveSpecialChar.removeSpecialChars(title);
					title = title.trim();
					query.clear();
					query.put("link", authorityUrl);
					cursor = sampleCrawlDataDB.find(query);
					if (!cursor.hasNext()) {
						if (!title.isEmpty()) {
							if (!Character.isDigit(title.charAt(0))) {

								filters = true;
								nofilters = true;
								if (classEnabled == false) {
									noRegexString = noRegex.trim();
									noRegexarray = noRegexString.split(" ");

									for (int k = 0; k < noRegexarray.length; k++) {

										if (!authorityUrl.contains(noRegexarray[k])) {
											nofilters = true;
										} else {
											nofilters = false;
											break;
										}
									}
									if (regex.contains("(^.+?\\d$)")) {
										if (authorityUrl.matches("(^.+?\\d$)")) {
											filters = true;
										} else
											filters = false;
									} else if (regex.contains("(.*(")) {
										// System.out.println("Initial value is
										// " +
										// filters);
										if (authorityUrl.matches(regex)) {
											filters = true;
											// System.out.println("Filters is
											// true");
										} else {
											filters = false;
											// System.out.println("Filters is
											// false");
										}
									} else if (regex.endsWith("$")) {
										if (authorityUrl.matches(regex)) {
											filters = true;
											// System.out.println("Filters is
											// true");
										} else {
											filters = false;
											// System.out.println("Filters is
											// false");
										}

									} else {
										if (authorityUrl.contains(regex)) {
											filters = true;
										} else
											filters = false;
									}
								}
								if (filters == true && nofilters == true) {

									nameFlag = true;
									for (int g = 0; g < nameStructure.size(); g++) {
										// System.out.println(title+"
										// checking for nameStructure
										// "+nameStructure.get(g));

										smallTitle = title.toLowerCase();
										smallNameStructure = nameStructure.get(g).toLowerCase();
										if (smallTitle.contains(smallNameStructure)) {
											nameFlag = false;
											break;
										} else {
											nameFlag = true;
										}
									}
									if (nameFlag == true) {
										/*
										 * System.out.println(title +
										 * " is clear");
										 * System.out.println(authorityUrl +
										 * " is clear");
										 */

										if (RobotChecks.metaRobots(doc).contentEquals("index")) {
											// System.out.println(authorityUrl+"
											// is indexable");
											if (authorityUrl.contains("?")) {
												authorityUrl = authorityUrl.substring(0, authorityUrl.lastIndexOf("?"));
											}

											int id = (int) getNextSequence("userid");
											insertObject.put("_id", id);
											insertObject.put("domainLink", domain);
											insertObject.put("seedUrl", URL);
											insertObject.put("link", authorityUrl);
											insertObject.put("title", RemoveSpecialChar.removeSpecialChars(title));
											insertObject.put("description", "");
											insertObject.put("mainCategory", "");
											insertObject.put("subCategory", type);
											insertObject.put("moduleName", moduleName);
											dNow = new Date();
											insertObject.put("systemDate", ft.format(dNow));
											insertObject.put("location", "");
											insertObject.put("translatedTitle", "");
											insertObject.put("translatedKeyword", "");
											insertObject.put("translatedDescription", "");
											insertObject.put("industry", "");
											insertObject.put("personName", "");
											insertObject.put("companyName", "");
											insertObject.put("followersCount", "");
											insertObject.put("leadDate", "");
											insertObject.put("geoLocation", "");
											insertObject.put("mainContent", "");
											insertObject.put("imageLocalPath", "");
											insertObject.put("expiredDate", "");
											insertObject.put("expiredStatus", "live");
											insertObject.put("keyword", "");
											insertObject.put("image", "no.jpg");
											insertObject.put("authorityTitle", "");
											insertObject.put("status", "false");
											insertObject.put("authorityStatus", "false");
											insertObject.put("boilerpipeStatus", "false");
											insertObject.put("nerStatus", "false");
											insertObject.put("imageStatus", "true");
											insertObject.put("translationStatus", "true");
											insertObject.put("geoip",
													"{'type' : 'MultiPoint','coordinates' : [[ 0.0, 0.0 ]]}");
											crawldatadb.insert(insertObject);
											sampleCrawlDataDB.insert(insertObject);
											System.out.println("SeedUrl is *******" + URL + "******" + moduleName);
											System.out.println("Authority is $$$$$$" + authorityUrl + "$$$$$$" + type);
											System.out.println("Title is---------"
													+ RemoveSpecialChar.removeSpecialChars(title) + "-----------");
											insertObject.clear();
										} else {
											System.out.println(authorityUrl + "is not indexable");
										}
									} else {
										System.out.println(title + " is not clear.");
									}
								} else {
									System.out.println(authorityUrl + " is not clear");
								}
							} else {
								System.out.println(title + " starts with number");
							}
						} else {
							System.out.println("Title is blank");
						}
					} else {
						System.out.println(authorityUrl + " is double by link");
					}

				}

			} catch (DuplicateKeyException e) {

				System.out.println("Duplicate data found....");
			}

			catch (HttpStatusException e1) {
				System.out.println("HTTP status exception while connecting " + URL);
			} catch (SocketTimeoutException e3) {
				System.out.println("Socket timeout exception while connecting to " + URL);
			} catch (SocketException e2) {
				System.out.println("Socket exception while connecting " + URL);
			} catch (Exception e) {
				// System.out.println("Some general error while connecting " +
				// URL);
				e.printStackTrace();
			}
		}
	}

	public static Object getNextSequence(String name) throws Exception {

		BasicDBObject find = new BasicDBObject();
		find.put("_id", name);
		BasicDBObject update = new BasicDBObject();
		update.put("$inc", new BasicDBObject("seq", 1));
		DBObject obj = counters.findAndModify(find, update);
		return obj.get("seq");
	}

	public static void updatenextCrawlDate(String url, int bucketNumber, String prevCrawlDate) throws ParseException {

		bucketNumber = bucketNumber + 1;
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		Calendar c = Calendar.getInstance();
		c.setTime(sdf.parse(prevCrawlDate));
		c.add(Calendar.DATE, bucketNumber); // number of days to add
		String nextCrawlDate = sdf.format(c.getTime());

		BasicDBObject seedUrlQuery = new BasicDBObject();
		seedUrlQuery.put("url", url);
		DBCursor testItemsCursor = seedUrl.find(seedUrlQuery);
		while (testItemsCursor.hasNext()) {
			DBObject updateItem = testItemsCursor.next();
			updateItem.put("nextCrawlDate", nextCrawlDate);
			seedUrl.save(updateItem);

		}
	}
}
