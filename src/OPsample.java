import java.util.ArrayList;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import com.mongodb.BasicDBObject;
import com.mongodb.DB;
import com.mongodb.DBCollection;
import com.mongodb.DBCursor;
import com.mongodb.DBObject;
import com.mongodb.MongoClient;

public class OPsample {
	
	static DB db = null;
	static DBCollection sampleCrawldatadb = null;
	static DBCollection crawldatadb = null;
	
	static{
		MongoClient mongoClient = new MongoClient("localhost", 27017);

		// Now connect to your databases
		db = mongoClient.getDB("crawler");
		System.out.println("Connected to database successfully");
		sampleCrawldatadb = db.getCollection("sampleCrawldata");
		crawldatadb = db.getCollection("crawldata");
		System.out.println("Both collections selected successfully");
	}
	public static void main(String[] args) {
		
		long startTime = System.nanoTime();
		DBCursor cursor = sampleCrawldatadb.find(); // If we do not have query
													// then keep brackets blank.
													// It will fetch all records
		cursor.addOption(com.mongodb.Bytes.QUERYOPTION_NOTIMEOUT);
		int count = 0;
		ArrayList<demo> list = new ArrayList<>();
		while (cursor.hasNext()) {
			DBObject obj = cursor.next();
			String link = obj.get("link").toString();
			String nospaceTitleDescription = obj.get("nospaceTitleDescription")
					.toString();
			//new Thread(new runnable(++count , nospaceTitleDescription)).start();
			if(list.size() == 1)
			{
				count = count+1;
				System.out.println("Set number "+count);
				System.out.println("List is full and calling threads "+list.size());
				new OPsample().intiate(list);
				list.clear();
				System.out.println("List size after clear is "+list.size());
			}
			else
			{
				list.add(new demo(link, nospaceTitleDescription));
			}
		}

		long endTime = System.nanoTime();
		long duration = (endTime - startTime); // divide by 1000000 to get
												// milliseconds.
		long time = duration / 1000000;
		System.out.println("Time taken is " + time);
	}
	
	public void intiate(ArrayList<demo> list)
	{

		ExecutorService executor = Executors.newFixedThreadPool(100);
		for (int i = 0; i < list.size(); i++) {
			
			String nospaceTitleDescription = list.get(i).nospaceTitleDescription;
			String link = list.get(i).link;

			MyRunnable worker = new MyRunnable(link,nospaceTitleDescription);
			executor.execute(worker);
		}
		executor.shutdown();
		// Wait until all threads are finish
		while (!executor.isTerminated()) {

		}
		System.out.println("\nFinished all threads");

	
	}
	
	public static class MyRunnable implements Runnable {
		String nospaceTitleDescription;
		String link;
		
		public MyRunnable(String link,String nospaceTitleDescription) {
			// TODO Auto-generated constructor stub
			this.link = link;
			this.nospaceTitleDescription = nospaceTitleDescription;
		}

		@Override
		public void run() {

			/*BasicDBObject query = new BasicDBObject();
			query.put("nospaceTitleDescription", nospaceTitleDescription);
			DBCursor cursor1 = sampleCrawldatadb.find(query);
			System.out.println("Gotacha "+cursor1.size());*/
			//if (cursor1.size() > 1)
			String nospaceTitleDescription1 = nospaceTitleDescription.substring(1, nospaceTitleDescription.length());
			if(!nospaceTitleDescription1.matches("[0-9]+"))
			{
				//int i = 1;
				//while (cursor1.hasNext()) 
				{

					/*DBObject obj1 = cursor1.next();
					String link1 = obj1.get("link").toString();
					String nospaceTitleDescription1 = obj1.get(
							"nospaceTitleDescription").toString();*/
					BasicDBObject updateFields = new BasicDBObject();
					//nospaceTitleDescription1 = nospaceTitleDescription1 + Integer.toString(i);
					//i++;
					long hashcode = nospaceTitleDescription.hashCode();
					String hash = String.valueOf(hashcode);
					updateFields.append("nospaceTitleDescription",
							hash);
					BasicDBObject setQuery = new BasicDBObject();
					setQuery.append("$set", updateFields);
					BasicDBObject searchQuery = new BasicDBObject().append("link",
							link);
					crawldatadb.update(searchQuery, setQuery);
					sampleCrawldatadb.update(searchQuery, setQuery);
					System.out.println("Done for " + link + " "+hash);
				}
			}
			else{
				System.out.println("Its number "+link+" "+nospaceTitleDescription1);
			}
		}

	}
	
}

class demo 
{
	public String link,nospaceTitleDescription;

	public demo(String link, String nospaceTitleDescription) {
		super();
		this.link = link;
		this.nospaceTitleDescription = nospaceTitleDescription;
	}
	
	
}