import com.mongodb.BasicDBObject;
import com.mongodb.DB;
import com.mongodb.DBCollection;
import com.mongodb.DBCursor;
import com.mongodb.DBObject;
import com.mongodb.MongoClient;

public class sample {
	public static void main(String[] args) {
		DB db = null;
		DBCollection sampleCrawldatadb = null;
		DBCollection crawldatadb = null;
		String collectionName="sampleCrawldata";
		try {

			// To connect to mongodb server
			MongoClient mongoClient = new MongoClient("localhost", 27017);

			// Now connect to your databases
			db = mongoClient.getDB("crawler");
			System.out.println("Connected to database successfully");
			sampleCrawldatadb = db.getCollection(collectionName);
			crawldatadb = db.getCollection("crawldata");
			System.out.println("Collection " + collectionName + " selected successfully");
			
			//To add document in the database
/*			BasicDBObject doc1 = new BasicDBObject();
			doc1.put("cid", "7");
			doc1.put("name", "Ajit");
			coll.insert(doc1);
			doc1.clear();	//It needs to get clear to update next records;
			doc1.put("cid", 10);
			doc1.put("name", "Chitti");
			coll.insert(doc1);
			doc1.clear();	//It needs to get clear to update next records;
*/
			//Select query in mongodb.
			//BasicDBObject query = new BasicDBObject();
			//query.put("nospaceTitleDescription", "");	//parameterized query. here we provide the condition
			DBCursor cursor = sampleCrawldatadb.find();	//If we don not have query then keep brackets blank. It will fetch all records
			int count=0,cnt=0;	
			cursor.addOption(com.mongodb.Bytes.QUERYOPTION_NOTIMEOUT);
			while(cursor.hasNext()) {
				System.out.println(count++);
				DBObject obj = cursor.next();
			    String link = obj.get("link").toString();
			    String nospaceTitleDescription = obj.get("nospaceTitleDescription").toString();
			    BasicDBObject query = new BasicDBObject();
				query.put("nospaceTitleDescription", nospaceTitleDescription);
				DBCursor cursor1 = sampleCrawldatadb.find(query);
				if(cursor1.size()>1)
				{
					int i=1;
					while(cursor1.hasNext())
					{
						
						DBObject obj1 = cursor1.next();
						String link1 = obj1.get("link").toString();
						String nospaceTitleDescription1 = obj1.get("nospaceTitleDescription").toString();
						BasicDBObject updateFields = new BasicDBObject();
						nospaceTitleDescription1 = nospaceTitleDescription1 + Integer.toString(i);
						i++;
					    updateFields.append("nospaceTitleDescription", nospaceTitleDescription1);
					    BasicDBObject setQuery = new BasicDBObject();
						setQuery.append("$set", updateFields);
						BasicDBObject searchQuery = new BasicDBObject().append("link", link1);
						crawldatadb.update(searchQuery, setQuery);
						sampleCrawldatadb.update(searchQuery, setQuery);
						System.out.println("Done for "+link1);
					}
				}
			}

/*			//Update query in mongodb
			BasicDBObject findTestItemQuery = new BasicDBObject();
			findTestItemQuery.put("cid", "7");
			DBCursor testItemsCursor = coll.find(findTestItemQuery);
			while(testItemsCursor.hasNext()) {
			    DBObject testCodeItem = testItemsCursor.next();
			    testCodeItem.put("name", "MS Dhoni");
			    coll.save(testCodeItem);

			}

			//Delete query in mongodb
			BasicDBObject deleteQuery = new BasicDBObject();
			deleteQuery.put("cid", "10");
			DBCursor cursor1 = coll.find(deleteQuery);
			while (cursor1.hasNext()) {
			    DBObject item = cursor1.next();
			    coll.remove(item);
			}
*/

		} catch (Exception e) {
			System.err.println(e.getClass().getName() + ": " + e.getMessage());
		}
	}
}
