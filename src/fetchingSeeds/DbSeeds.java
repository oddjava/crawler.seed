package fetchingSeeds;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Random;
import java.util.regex.Pattern;

import com.mongodb.BasicDBObject;
import com.mongodb.DB;
import com.mongodb.DBCollection;
import com.mongodb.DBCursor;
import com.mongodb.DBObject;
import com.mongodb.MongoClient;

import mongoDBConnection.GetConnection;

public class DbSeeds {

	public String url, domainName, moduleName, nextCrawlDate, type, description, keyword, regex, noRegex,
			subMain = null;
	public int bucketNumber;
	final static int size = 500;

	public DbSeeds(String url, String domainName, String moduleName, String nextCrawlDate, int bucketNumber,
			String type, String description, String keyword, String regex, String noRegex, String subMain) {
		super();
		this.url = url;
		this.domainName = domainName;
		this.moduleName = moduleName;
		this.nextCrawlDate = nextCrawlDate;
		this.bucketNumber = bucketNumber;
		this.type = type;
		this.description = description;
		this.keyword = keyword;
		this.regex = regex;
		this.subMain = subMain;
		this.noRegex = noRegex;
	}

	public static DBCursor dbSeedUrl() {
		String domainName, description = "", keyword = "", regex = "", noRegex = "";

		DBCollection domainDb = GetConnection.connect("domain"); // connected to
																	// domain
																	// collection
		DBCollection seedurlDb = GetConnection.connect("SeedUrl"); // connected
																	// to
																	// seedurl
																	// collection

		BasicDBObject domainQuery1 = new BasicDBObject();
		List<BasicDBObject> list1 = new ArrayList<BasicDBObject>();
		list1.add(new BasicDBObject("regex", new BasicDBObject("$ne", "no regex")));
		domainQuery1.put("$and", list1);
		DBCursor domain1Cursor = domainDb.find(domainQuery1);
		List<BasicDBObject> list = new ArrayList<BasicDBObject>();
		int count = 0;
		while (domain1Cursor.hasNext()) {
			count++;
			DBObject obj = domain1Cursor.next();
			keyword = obj.get("keyword").toString();
			description = obj.get("description").toString();
			regex = obj.get("regex").toString();
			noRegex = obj.get("noRegex").toString();
			domainName = obj.get("domainName").toString();
			list.add(new BasicDBObject("domainUrl", domainName));
		}

		BasicDBObject orquery = new BasicDBObject();
		orquery.put("systemName", Pattern.compile(Pattern.quote("NER")));
		orquery.put("$or", list);
		DBCursor cursor = seedurlDb.find(orquery);

		return cursor;

	}
}

class seedData {
	public String url, domainName, moduleName, nextCrawlDate, type, description, keyword, regex, submain, noRegex;
	public int bucketNumber;

	public seedData(String url, String domainName, String moduleName, String nextCrawlDate, int bucketNumber,
			String type, String description, String keyword, String regex, String noRegex, String submain) {
		super();
		this.url = url;
		this.domainName = domainName;
		this.moduleName = moduleName;
		this.nextCrawlDate = nextCrawlDate;
		this.bucketNumber = bucketNumber;
		this.type = type;
		this.description = description;
		this.keyword = keyword;
		this.regex = regex;
		this.submain = submain;
		this.noRegex = noRegex;
	}
}